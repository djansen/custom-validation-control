﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CustomValidationControl;

namespace CustomValidationWebForms
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

// ReSharper disable once InconsistentNaming
        protected void cbOverrideValidation_Checked(object sender, EventArgs eventArgs)
        {
            if(cbOverrideValidation.Checked)
            {
                btnSave.CausesValidation = false;
                lblOverrideValidation.Text = "On";
                return;
            }
            btnSave.CausesValidation = true;
            lblOverrideValidation.Text = "Off";
        }

        protected void Save_OnClick(object sender, EventArgs eventArgs)
        {
            DoCustomValidation(cbOverrideValidation.Checked);

        }

        protected void ddlOverrideValidationReasons_SelectedIndexChanged(object sender, EventArgs eventArgs)
        {
            

        }

        protected void OverrideValidation_Click(object sender, CommandEventArgs eventArgs)
        {
            string command = eventArgs.CommandName;
            switch (command.ToLower())
            {
                case "delete":
                    DeleteOverride(eventArgs.CommandArgument.ToString());
                    break;
                case "edit":
                    break;
                default:
                    break;
            }
        }

        private void DeleteOverride(string InvalidReasonId)
        {

        }

// ReSharper disable once InconsistentNaming
        private void DoCustomValidation(bool overrideValidation)
        {
            Page.Validate();

            foreach (IValidator validator in Validators)
            {
                validator.IsValid = overrideValidation;

                if(validator is ICustomValidator)
                {
                    SaveDefectMessage((ICustomValidator)validator);
                }
            }
        }

        private void DeleteValidationErrorsBySection(string SectionId)
        {
            
        }

        private void SaveDefectMessage(ICustomValidator customValidator)
        {
            var validatorControl = (BaseValidator)customValidator;
            
            // TODO: Save extra info to a database. 
        }
    }
}