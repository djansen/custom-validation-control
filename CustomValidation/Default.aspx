﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CustomValidationWebForms._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %>.</h1>
                <h2></h2>
            </hgroup>
            <p>
                
            </p>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h3>Custom Validator Controls</h3>
    
    
    <label for='<% = cbOverrideValidation.ClientID %>'>Override Validation <asp:Label ID="lblOverrideValidation" Text="Off" runat="server"/></label> 
    <asp:CheckBox ID="cbOverrideValidation" AutoPostBack="True" OnCheckedChanged="cbOverrideValidation_Checked" runat="server"/>
    
    <label for='<% = tbText.ClientID %>'>Text:</label>
    <asp:TextBox ID="tbText" runat="server"></asp:TextBox>
    
    <ccc:CustomRequiredFieldValidator ControlToValidate="tbText" 
        ValidationId="SomeKey123"
        DefectReason="Some Defect Reason" 
        Cure="Some cure" 
        Section="1" 
        ErrorMessage="Text is required" runat="server"/>

    <asp:Button ID="btnSave" runat="server" Text="Save & Validate" OnClick="Save_OnClick"/>
    

    
    
    <label for='<% = ddlOverrideValidationReasons.ClientID %>'></label>
    <asp:DropDownList ID="ddlOverrideValidationReasons" 
        OnSelectedIndexChanged="ddlOverrideValidationReasons_SelectedIndexChanged" 
        AutoPostBack="True" runat="server">
        <asp:ListItem Value="1">Please Choose</asp:ListItem>
        <asp:ListItem Value="2">It was long looking me.</asp:ListItem>
        <asp:ListItem Value="3">Smelled funny.</asp:ListItem>
        <asp:ListItem Value="4">Other</asp:ListItem>
    </asp:DropDownList>
    
    <asp:Panel ID="pnlOther" Visible="False" runat="server">
        <label for="<% = tbOther.ClientID %>" >Other:</label>
        <asp:TextBox ID="tbOther" runat="server"></asp:TextBox>
    </asp:Panel>
    
    <asp:Repeater ID="rptrOverrideValidations" OnItemCommand="OverrideValidation_Click" runat="server">
        <HeaderTemplate>
            <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <li><%# DataBinder.Eval(Container.DataItem, "ReasonName") %> 
                <asp:LinkButton Text="(x)" CommandName="Delete" runat="server"
                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ReasonId") %> ' /></li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>

    
</asp:Content>
