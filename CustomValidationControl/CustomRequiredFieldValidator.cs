﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace CustomValidationControl
{
    public class CustomRequiredFieldValidator : System.Web.UI.WebControls.RequiredFieldValidator, ICustomValidator
    {
        public string ValidationId { get; set; }

        public string Section { get; set; }
        
        public string DefectReason { get; set; }

        public string Cure { get; set; }
        
    }
}
