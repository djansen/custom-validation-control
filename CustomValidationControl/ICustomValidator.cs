﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomValidationControl
{
    public interface ICustomValidator 
    {
        string ValidationId { get; set; }

        string Section { get; set; }

        string DefectReason { get; set; }

        string Cure { get; set; }

    }
}
